from jinja2 import Environment, FileSystemLoader

name = 'templates'

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

template = env.get_template('test.html')

output = template.render(name=name)
print(output)