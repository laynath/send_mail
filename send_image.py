from pathlib import Path
from email.mime.image import MIMEImage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

image_path = '/home/msi/Desktop/send_mail/testData/my_image.png'
image_name = Path(image_path).name

# the function for sending an email
def send_email(recipient, 
               sender,
               subject, 
               text_content,
               image_path=image_path,
               image_name=image_name):

    html_content=f"""<!doctype html>
    <html lang=en>
        <head>
            <meta charset=utf-8>
            <title>Some title.</title>
        </head>

        <body>
            <h2>{subject}</h2>
            <p>
            Here is my nice image.<br>
            <img src='cid:{image_name}'/>
            </p>
        </body>
    </html>"""
    
    email = EmailMultiAlternatives(subject=subject, body=text_content, 
            from_email=sender, to=recipient if isinstance(recipient, list) else [recipient])

    if all([html_content,image_path,image_name]):
        email.attach_alternative(html_content, "text/html")
        email.content_subtype = 'html'  # set the primary content to be text/html
        email.mixed_subtype = 'related' # it is an important part that ensures embedding of an image 

        with open(image_path, mode='rb') as f:
            image = MIMEImage(f.read())
            email.attach(image)
            image.add_header('Content-ID', f"<{image_name}>")

    email.send()

