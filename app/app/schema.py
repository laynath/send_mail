import graphene
import graphql_jwt

# import tracks.schema
import user.schema
import sendemail.schema as sendemail
import telegram_mails.schema as tele

class Query(user.schema.Query, graphene.ObjectType):
    pass

class Mutation(tele.Mutation, sendemail.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)