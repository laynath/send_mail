from app.settings import API_ID,API_HASH
from telethon import TelegramClient
import os

def register(phone):
    try:
        client = TelegramClient(phone, API_ID, API_HASH)
        client.connect()
        phone_code_hash = client.send_code_request(phone).phone_code_hash
        f= open(phone,"w")
        f.write(phone_code_hash)
        f.close() 
    except:
       raise Exception("Register Error") 


def getcode(phone, code):
    try:
        f= open(phone,"r")
        phone_code_hash = f.read()
        os.remove(phone)
        f.close()
        client = TelegramClient(phone, API_ID, API_HASH)
        client.connect()
        client.sign_in(phone, code, phone_code_hash=phone_code_hash)
    except:
       raise Exception('Logged in error!') 