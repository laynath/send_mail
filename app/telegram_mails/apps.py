from django.apps import AppConfig


class TelegramMailsConfig(AppConfig):
    name = 'telegram_mails'
