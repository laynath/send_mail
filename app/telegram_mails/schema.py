from graphene_django import DjangoObjectType
import graphene
from telegram_mails.login import register, getcode
from telegram_mails.telemail import sendMessage



class TelegramResgister(graphene.Mutation):
    ok = graphene.Boolean()
 
    class Arguments:
        phone = graphene.String() 

    def mutate(self, info, phone):
        register(phone=phone)
        return TelegramResgister(ok=True)


class TelegramLogin(graphene.Mutation):
    ok = graphene.Boolean()
 
    class Arguments:
        phone = graphene.String() 
        code = graphene.Int()

    def mutate(self, info, phone, code):
        getcode(phone=phone, code=code)
        return TelegramLogin(ok=True)



class TelegramMessage(graphene.Mutation):
    ok = graphene.Boolean()

    class Arguments:
        phone = graphene.String() 
        channel = graphene.String()
        message = graphene.String()
    
    def mutate(self, info, phone,  channel, message):
        sendMessage(phone=phone,
                   channel=channel,
                   message=message
                   )
        return TelegramMessage(ok=True)


class Mutation(graphene.ObjectType):
    telegram_resgister = TelegramResgister.Field()
    telegram_login = TelegramLogin.Field()
    telegram_message = TelegramMessage.Field()