from app.settings import API_ID,API_HASH
from telethon import TelegramClient
import time


def sendMessage(phone, channel, message):
    try:
        client = TelegramClient(phone, API_ID, API_HASH)
        client.connect()
        if not client.is_user_authorized():
            raise Exception("Please Register First")
        participants = client.get_participants(channel)

        me = client.get_me()

        if len(participants):
            for user in participants:
                if me.id==user.id:
                    pass
                else:
                    client.send_message(user.id,  message=message)
                    time.sleep(5)
    except:
       raise Exception("Sending Error")