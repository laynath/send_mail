import os, ssl
ssl._create_default_https_context = ssl._create_unverified_context

from django.core.mail import send_mail
from jinja2 import Environment, FileSystemLoader



def sendmail(email, subject, context, name, fname):
    # fname = "temp.html"
    file_loader = FileSystemLoader('templates')
    env = Environment(loader=file_loader)
    template = env.get_template(fname)
    msg = template.render(name=name, message=context)
    send_mail(
        subject=subject,
        message=msg,
        from_email='laynath@gmail.com',
        recipient_list=[email],
        fail_silently=True,
    )

