import graphene
import os, json
from graphene_django import DjangoObjectType
from sendemail.send_email import sendmail
from app.settings import DEFAULT_FROM_EMAIL


class SendEmail(graphene.Mutation):
    ok = graphene.Boolean()

    class Arguments:
        subject = graphene.String()
        context = graphene.String()
        fname = graphene.String()


    def mutate(self, info, subject, context, fname="base/temp.html"):
        with open('../emailList/email-list.json', 'r') as file:
            data=file.read()
        obj = json.loads(data)
        try:
            for key in obj:
                name = key['firstname'] +' '+ key['lastname']
                email = key['email']
                sendmail(email=email,subject=subject, context=context, name=name, fname=fname)
        except:
            raise Exception('Sending Fail..')
        return SendEmail(ok=True)


class Mutation(graphene.ObjectType):
    send_email = SendEmail.Field()

