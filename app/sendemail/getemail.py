# import re


# findMail = re.compile((r"([a-z0-9!#$%&*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
#                     r"{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
#                     r"\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))

# def get_emails(s):
#     return (email[0] for email in re.findall(findMail, s)
#         if not email[0].startswith('//'))


from jinja2 import Environment, FileSystemLoader

name = 'templates'

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

template = env.get_template('test.html')

output = template.render(name=name)
print(output)